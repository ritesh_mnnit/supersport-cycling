﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cycling.Tests
{
    [TestFixture]
    public class Service1Tests
    {
        [Test]
        public void Riders_MakingASingleCall_TheCallSucceeds()
        {
            // ARRANGE

            Service1 target = new Service1();

            // ACT

            target.Riders();

            // ASSERT
        }

        [Test]
        public void ProcessFiles_MakingASingleCall_TheCallSucceeds()
        {
            // ARRANGE

            Service1 target = new Service1();

            // ACT

            target.ProcessFiles();

            // ASSERT
        }

        [Test]
        public void GetFiles_MakingASingleCall_TheCallSucceeds()
        {
            // ARRANGE

            Service1 target = new Service1();

            // ACT

            target.GetFiles();

            // ASSERT
        }

        [Test]
        public void Run_MakingASingleCall_TheCallSucceeds()
        {
            // ARRANGE

            Service1 target = new Service1();

            // ACT

            target.Run();

            // ASSERT
        }
    }
}
