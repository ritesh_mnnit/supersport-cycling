﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cycling
{
    public class runInfo
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Downloads;
        public int DownloadErrors;
        public List<string> Errors { get; set; }
    }
}