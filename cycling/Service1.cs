﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using System.Globalization;
using System.Net;
using Common.Logging;
using System.Text.RegularExpressions;

namespace cycling
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer SysTimer;
        public string DbConnString = ConfigurationManager.AppSettings["SQL"];
        public string DbTestConnString = ConfigurationManager.AppSettings["TestSQL"];
        public SqlConnection DbConn;
        public bool DbConnected = true;
        public runInfo CurRun;
        public int Tournament = -1;
        private bool _test = true;
        private DateTime _stagesLastRun = DateTime.Now.AddDays(-2);
        private readonly int _year = Convert.ToInt16(ConfigurationManager.AppSettings["tdfYear"]);
        private readonly string _tdfStart = ConfigurationManager.AppSettings["tdfStart"];
        private readonly string _tdfEnd = ConfigurationManager.AppSettings["tdfEnd"];
        private readonly string _tourName = ConfigurationManager.AppSettings["tdfTourName"];
        private readonly string _pollingStart = ConfigurationManager.AppSettings["hourOfDayToStartPolling"];
        private readonly string _pollingEnd = ConfigurationManager.AppSettings["hourOfDayToEndPolling"];

        

        private ILog _logger = null;

        public Service1()
        {
            InitializeComponent();

            _logger = LogManager.GetLogger(typeof(Service1));
        }

        internal void TestStartupAndStop(string[] args)
        {
            _test = false;
            Run();
            SysTimer = new System.Timers.Timer(5000);
            SysTimer.Elapsed += TimerElapsed;
            SysTimer.AutoReset = false;
            SysTimer.Enabled = true;
            //Console.ReadLine();
            Thread.Sleep(600000);
        }

        protected override void OnStart(string[] args)
        {
            _test = false;
            SysTimer = new System.Timers.Timer(5000);
            SysTimer.Elapsed += TimerElapsed;
            SysTimer.AutoReset = false;
            SysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void DbConnChanged(object sender, EventArgs e)
        {
            if (!DbConnected || DbConn == null) return;
            if (DbConn.State != ConnectionState.Broken && DbConn.State != ConnectionState.Closed) return;
            try
            {
                DbConn = new SqlConnection(DbConnString);
                DbConn.Open();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in dbConnChanged()", ex);

                DbConnected = false;
            }
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            var t = new Thread(Run);
            t.IsBackground = true;
            t.Start();
            
        }

        public void Run()
        {

            Rebex.Licensing.Key = "==AV0STJewgFcBQTI01f23/0nBW4x1+QBmujLYVbWAoTyQ==";

            if (DateTime.Now.Hour >= Convert.ToInt16(_pollingStart) && DateTime.Now.Hour <= Convert.ToInt16(_pollingEnd))
            {
                CurRun = new runInfo
                {
                    Start = DateTime.Now,
                    Errors = new List<string>()
                };

                try
                {
                    if (_test)
                    {
                        DbConn = new SqlConnection(DbTestConnString);
                        DbConn.Open();
                    }
                    else
                    {
                        DbConn = new SqlConnection(DbConnString);
                        DbConn.Open();
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in Run()", ex);

                    DbConnected = false;
                    CurRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }

                if (DbConnected && DbConn != null)
                {
                    try
                    {
                        DbConn.StateChange += new System.Data.StateChangeEventHandler(DbConnChanged);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in Run()", ex);

                        CurRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    }

                    try
                    {
                        TimeSpan lastCheck = DateTime.Now.Subtract(_stagesLastRun);
                        if (lastCheck.TotalHours > 24)
                        {
                            var startOfCheck = DateTime.Now;
                            var checkTourExist =
                                new SqlCommand(
                                    "SELECT Count(id) FROM SSZGeneral.dbo.tourdefrance WHERE (Year(StartDate) = " +
                                    _year + ")", DbConn);
                            var tourExist = Convert.ToBoolean(checkTourExist.ExecuteScalar());
                            if (!tourExist)
                            {
                                try
                                {
                                    var startDate = _tdfStart.Split('/');
                                    var tourStart = new DateTime(Convert.ToInt32(startDate[0]),
                                        Convert.ToInt32(startDate[1]), Convert.ToInt32(startDate[2]));

                                    var endDate = _tdfEnd.Split('/');
                                    var tourEnd = new DateTime(Convert.ToInt32(endDate[0]), Convert.ToInt32(endDate[1]),
                                        Convert.ToInt32(endDate[2]));

                                    var insertTour =
                                        new SqlCommand(
                                            "INSERT INTO SSZGeneral.dbo.tourdefrance (Name, StartDate, EndDate, Created) VALUES ('" +
                                            _tourName + "', '" + tourStart + "', '" + tourEnd + "', '" + DateTime.Now +
                                            "')", DbConn);
                                    insertTour.ExecuteScalar();

                                    var getTournament = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                                    Tournament = Convert.ToInt32(getTournament.ExecuteScalar());
                                }
                                catch (SqlException ex)
                                {
                                    _logger.Error("An Error occurred while creating new season.", ex);
                                    CurRun.Errors.Add("Failed to create new season");
                                }
                            }

                            GetStagesFile();
                            GetTeamsFile();
                            GetRidersFile();

                            var endOfCheck = DateTime.Now;
                            StringBuilder extendedInfo = new StringBuilder();
                            extendedInfo.AppendLine(
                                "    Previous Tournament/Stages Check: " + _stagesLastRun.ToString("yyyy-MM-dd HH:mm:ss"));
                            extendedInfo.Append(
                                "    Duration of Tournament/Stages Check (in seconds): " +
                                endOfCheck.Subtract(startOfCheck).TotalSeconds);
                            WriteLog(extendedInfo.ToString(), startOfCheck, endOfCheck, "");
                            _stagesLastRun = DateTime.Now;
                        }

                        var getTournamentId = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                        Tournament = Convert.ToInt32(getTournamentId.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in Run()", ex);

                        CurRun.Errors.Add("Failed to get latest tournament id");
                    }

                    if (CurRun.Errors.Count <= 0)
                    {
                        GetFiles();
                        ProcessFiles();
                    }

                    try
                    {
                        DbConn.StateChange -= DbConnChanged;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in Run()", ex);

                        CurRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    }
                }
                else
                {
                    CurRun.Errors.Add("Failed to connect to the database");
                }

                CurRun.End = DateTime.Now;
                EndRun();
                WriteInfo();

                DbConn?.Dispose();
            }

            SysTimer = new System.Timers.Timer(60000);
            SysTimer.Elapsed += TimerElapsed;
            SysTimer.AutoReset = false;
            SysTimer.Enabled = true;
        }

        private void GetRidersFile()
        {
            //var ridersExistForTou = new SqlCommand(
            //    "Select Count(*) FROM SSZGeneral.dbo.tourdefranceriders WHERE (Tournament = " + Tournament + ");",
            //    DbConn).ExecuteScalar();

            var ridersExistForTournament = new SqlCommand(
                "Select Count(*) FROM SSZGeneral.dbo.tourdefranceriders WHERE (Tournament = " + Tournament + ");",
                DbConn).ExecuteScalar();

            if (Tournament < 0)
            {
                var getTournamentId = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                Tournament = Convert.ToInt32(getTournamentId.ExecuteScalar());
            }

            if (!Convert.ToBoolean(ridersExistForTournament))
            {
                //Rebex.Net.Ftp curFtp = new Rebex.Net.Ftp();

                Rebex.Net.Sftp curFtp = new Rebex.Net.Sftp();

                string ftpServer = ConfigurationManager.AppSettings["ftpRBServer"];
                string ftpUser = ConfigurationManager.AppSettings["ftpRBUser"];
                string ftpPassword = ConfigurationManager.AppSettings["ftpRBPassword"];


                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.DownloadFileAsync(new System.Uri("http://www.letour.fr/xml/TDF-01.dtd"),
                            @"C:\SuperSport\Cycling\Temp\TDF-01.dtd");
                    }

                    //curFtp.Connect(ftpServer);
                    curFtp.Connect(ftpServer,2200);
                    curFtp.Login(ftpUser, ftpPassword);
                    curFtp.ChangeDirectory("sftp/tdf");
                    curFtp.GetFile("_TDF_" + _year + "_RIDERS.xml", @"C:\SuperSport\Cycling\Temp\" + "Riders.xml");

                    curFtp.Disconnect();

                    var done = Riders();

                    var Fi = new FileInfo(@"C:\SuperSport\Cycling\Temp\" + "Riders.xml");

                    if (done)
                    {
                        Fi.MoveTo("c:/SuperSport/Cycling/Processed/" + Fi.Name + DateTime.Now.ToString("_yyyy_MM_dd_HHmmss"));
                    }
                    else
                    {
                        Fi.MoveTo("c:/SuperSport/Cycling/Unprocessed/" + Fi.Name + DateTime.Now.ToString("_yyyy_MMMM_dd_HHmmss"));
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in GetStartersFile()", ex);

                    CurRun.Errors.Add(ex.Message);
                }
                finally
                {
                    curFtp?.Dispose();
                } 
            }
        }

        private void GetTeamsFile()
        {
            Rebex.Net.Sftp curFtp = new Rebex.Net.Sftp();
            string ftpServer = ConfigurationManager.AppSettings["ftpRBServer"];
            string ftpUser = ConfigurationManager.AppSettings["ftpRBUser"];
            string ftpPassword = ConfigurationManager.AppSettings["ftpRBPassword"];

            try
            {
                //curFtp.Connect(ftpServer);
                curFtp.Connect(ftpServer, 2200);
                //curFtp.Connect(2200);
                curFtp.Login(ftpUser, ftpPassword);
                curFtp.ChangeDirectory("sftp/tdf");
                curFtp.GetFile("_TDF_" + _year + "_STARTERS.xml", @"C:\SuperSport\Cycling\Temp\" + "_TDF_" + _year + "_STARTERS.xml");
                //curFtp.Get("_TDF_" + _year + "_STARTERS.xml", @"C:\SuperSport\Cycling\Temp\" + "_TDF_" + _year + "_STARTERS.xml");

                curFtp.Disconnect();

                ProcessFiles();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in GetStartersFile()", ex);

                CurRun.Errors.Add(ex.Message);
            }
            finally
            {
                curFtp?.Dispose();
            }
        }

        private void WriteLog(string extendedInfo, DateTime start, DateTime end, string errors)
        {
            try
            {
                FileStream Fs = new FileStream(ConfigurationManager.AppSettings["RunningLog"], FileMode.Append, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.End);
                Sw.WriteLine("Start: " + start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + end.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("Extended Info: ");
                Sw.WriteLine(extendedInfo);
                if(!string.IsNullOrEmpty(errors))
                {
                    Sw.WriteLine("Error: " + errors);
                }
                Sw.WriteLine("**********************************");
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in WriteLog()", ex);
            }
        }

        private void GetStagesFile()
        {

            #region temp
            _logger.Error("1", null);
            CurRun.Errors.Add("1"); 
            #endregion

            //Rebex.Net.Ftp curFtp = new Rebex.Net.Ftp();
            Rebex.Net.Sftp curFtp = new Rebex.Net.Sftp();

            #region temp
            _logger.Error("2", null);
            CurRun.Errors.Add("2");
            #endregion

            string ftpServer = ConfigurationManager.AppSettings["ftpRBServer"];
            string ftpUser = ConfigurationManager.AppSettings["ftpRBUser"];
            string ftpPassword = ConfigurationManager.AppSettings["ftpRBPassword"];
            
            if (_test)
            {
                #region temp
                _logger.Error("3", null);
                CurRun.Errors.Add("3");
                #endregion
                DbConn = new SqlConnection(DbTestConnString);
                DbConn.Open();

                #region temp
                _logger.Error("4", null);
                CurRun.Errors.Add("4");
                #endregion

                var getTournamentId = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                Tournament = Convert.ToInt32(getTournamentId.ExecuteScalar());
            }

            try
            {
                #region temp
                _logger.Error("5", null);
                CurRun.Errors.Add("5");
                #endregion

                curFtp.Connect(ftpServer, 2200);
                curFtp.Login(ftpUser, ftpPassword);
                curFtp.ChangeDirectory("sftp/tdf");

                #region temp
                _logger.Error("6", null);
                CurRun.Errors.Add("6");
                #endregion

                curFtp.GetFile("_TDF_"+ _year +"_STAGES.xml", @"C:\SuperSport\Cycling\Temp\" + "_TDF_" + _year + "_STAGES.xml");

                #region temp
                _logger.Error("7", null);
                CurRun.Errors.Add("7");
                #endregion

                curFtp.Disconnect();

                ProcessFiles();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in GetStagesFile()", ex);

                CurRun.Errors.Add(ex.Message);
            }
            finally
            {
                curFtp?.Dispose();
            }
        }

        public bool Riders()
        {
            var done = true;
            SqlCommand sqlQuery;

            if (_test)
            {
                DbConn = new SqlConnection(DbTestConnString);
                DbConn.Open();

                sqlQuery = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                Tournament = Convert.ToInt32(sqlQuery.ExecuteScalar());
            }

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(@"c:\SuperSport\Cycling\Temp\Riders.xml");
                xmlDoc.LoadXml(Regex.Replace(File.ReadAllText(@"c:\SuperSport\Cycling\Temp\Riders.xml"), "<!DOCTYPE.+?>", string.Empty));

                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("data/riders"))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("DELETE FROM SSZGeneral.dbo.tourdefranceriders WHERE (Tournament = " + Tournament + ");");

                    XmlNodeList xmlTeams = xmlNode.SelectNodes("team");
                    if (xmlTeams.Count > 0)
                    {
                        foreach (XmlNode teamNode in xmlTeams)
                        {
                            string teamNumber = string.Empty;
                            int teamId = -1;

                            foreach (XmlAttribute attribute in teamNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "number":
                                        sqlQuery = new SqlCommand("SELECT Id FROM SSZGeneral.dbo.tourdefranceteams WHERE (Number = @number) AND (Tournament = @tournament)", DbConn);
                                        sqlQuery.Parameters.Add("@number", SqlDbType.Int).Value = attribute.Value;
                                        sqlQuery.Parameters.Add("@tournament", SqlDbType.Int).Value = Tournament;
                                        teamId = Convert.ToInt32(sqlQuery.ExecuteScalar());
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode rider in teamNode.SelectNodes("rider"))
                            {
                                string firstName = string.Empty;
                                string lastName = string.Empty;
                                string pCaseLastName = string.Empty;
                                string number = string.Empty;
                                string nationality = string.Empty;
                                string status = string.Empty;

                                foreach (XmlAttribute attribute in rider.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "number":
                                            number = attribute.Value;
                                            break;
                                        case "lastname":
                                            lastName = attribute.Value;
                                            break;
                                        case "firstname":
                                            firstName = attribute.Value;
                                            break;
                                        case "nationality":
                                            nationality = attribute.Value;
                                            break;
                                        case "status":
                                            status = attribute.Value;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                                TextInfo textInfo = cultureInfo.TextInfo;
                                pCaseLastName = textInfo.ToTitleCase(lastName.ToLower());

                                if (teamId > 0)
                                {
                                    sb.Append("INSERT INTO SSZGeneral.dbo.tourdefranceriders (Tournament, Team, Number, FirstName, LastName, Nationality, Status, Created, tmpLastname, tmpName) VALUES (" + Tournament + ", " + teamId + ", '" + number.Replace("'", "''").Trim() + "', '" + firstName.Replace("'", "''").Trim() + "', '" + pCaseLastName.Replace("'", "''").Trim() + "', '" + nationality.Replace("'", "''").Trim() + "', '" + status.Replace("'", "''").Trim() + "', '" + DateTime.Now + "', '" + lastName.Replace("'", "''").Trim() + "', '" + firstName.Replace("'", "''").Trim() + " " + pCaseLastName.Replace("'", "''").Trim() + "')");
                                }
                            }
                        }
                    }

                    if (sb.ToString().IndexOf("INSERT") >= 0)
                    {
                        sqlQuery = new SqlCommand(sb.ToString(), DbConn);
                        sqlQuery.ExecuteNonQuery();
                    }
                }

                xmlDoc = null;

                if (_test)
                {
                    DbConn = new SqlConnection(DbTestConnString);
                    DbConn.Close();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return done;
        }

        public void ProcessFiles()
        {
            #region temp
            _logger.Error("8", null);
            CurRun.Errors.Add("8");
            #endregion

            SqlCommand SqlQuery;
            XmlDocument xmlDocument;

            if (_test)
            {
                DbConn = new SqlConnection(DbTestConnString);
                DbConn.Open();

                SqlQuery = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                Tournament = Convert.ToInt32(SqlQuery.ExecuteScalar());
            }

            foreach (string tmpFile in Directory.GetFiles("c:/SuperSport/Cycling/Temp"))
            {
                #region temp
                _logger.Error("9", null);
                CurRun.Errors.Add("9");
                #endregion

                FileInfo Fi = new FileInfo(tmpFile);

                if (Fi.Name.IndexOf("dtd") < 0)
                {
                    try
                    {
                        bool done = true;
                        xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml(Regex.Replace(File.ReadAllText("c:/SuperSport/Cycling/Temp/" + Fi.Name), "<!DOCTYPE.+?>", string.Empty));

                        if (Fi.Name.ToLower() == "riders.xml")
                        {
                            #region temp
                            _logger.Error("10", null);
                            CurRun.Errors.Add("10");
                            #endregion
                            done = Riders();
                        }
                        else
                        {
                            #region temp
                            _logger.Error("11", null);
                            CurRun.Errors.Add("11");
                            #endregion
                            done = Data(xmlDocument);
                        }

                        if (done)
                        {
                            Fi.CopyTo("c:/SuperSport/Cycling/Processed/" + Fi.Name +
                                      DateTime.Now.ToString("_yyyy_MM_dd_HHmmss"));
                        }
                        else
                        {
                            Fi.CopyTo("c:/SuperSport/Cycling/Unprocessed/" + Fi.Name +
                                      DateTime.Now.ToString("_yyyy_MMMM_dd_HHmmss"));
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in ProcessFiles()", ex);

                        CurRun.Errors.Add(ex.Message);
                        Fi.CopyTo("c:/SuperSport/Cycling/Unprocessed/" + Fi.Name +
                                  DateTime.Now.ToString("_yyyy_MMMM_dd_HHmmss"));
                    }
                    finally
                    {
                        Fi.Delete();
                    }
                }
            }
        }

        public void GetFiles()
        {
            SqlCommand SqlQuery;
            Rebex.Net.Sftp curFtp = new Rebex.Net.Sftp();
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"];
            string ftpUser = ConfigurationManager.AppSettings["ftpUser"];
            string ftpPassword = ConfigurationManager.AppSettings["ftpPassword"];
            string stageNumber = string.Empty;

            //Tamir.SharpSsh.Sftp curFtp = new Tamir.SharpSsh.Sftp(ftpServer, ftpUser, ftpPassword);

            if (_test)
            {
                DbConn = new SqlConnection(DbTestConnString);
                DbConn.Open();

                SqlQuery = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrance ORDER BY Startdate DESC", DbConn);
                Tournament = Convert.ToInt32(SqlQuery.ExecuteScalar());
            }

            try
            {
                SqlQuery = new SqlCommand("SELECT TOP 1 Number FROM SSZGeneral.dbo.TourdefranceStages WHERE (Tournament = " + Tournament + ") AND (Month(StartDate) = " + DateTime.Now.Month + ") AND (Day(StartDate) = " + DateTime.Now.Day + ")", DbConn);
                stageNumber = SqlQuery.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in GetFiles()", ex);

                CurRun.Errors.Add(ex.Message);
            }

            if (!String.IsNullOrEmpty(stageNumber))
            {
                try
                {
                   // curFtp.Connect(ftpServer);
                    curFtp.Connect(ftpServer, 2200);
                    //curFtp.Connect(2200);
                    curFtp.Login(ftpUser, ftpPassword);

                    //Rebex.Net.FtpList tmpList = new Rebex.Net.FtpList();
                    //tmpList = curFtp.GetFile();

                    Rebex.Net.SftpItemCollection tmpList = new Rebex.Net.SftpItemCollection();
                    tmpList = curFtp.GetList();

                    int total = tmpList.Count;
                    if (total > 0)
                    {
                        int Downloaded = 0;
                        int fileErrors = 0;
                        for (int i = 0; i <= total - 1; i++)
                        {
                            Rebex.Net.SftpItem curItem = tmpList[i];
                            if (curItem.Length > 0)
                            {
                                try
                                {
                                    if (curItem.Name.IndexOf(stageNumber) >= 0)
                                    {
                                        if (curItem.Name.StartsWith("SITG") || curItem.Name.StartsWith("SIPG") || curItem.Name.StartsWith("SIMG") || curItem.Name.StartsWith("SIJG") || curItem.Name.StartsWith("SICG") || curItem.Name.StartsWith("SETG") || curItem.Name.StartsWith("SITE") || curItem.Name.StartsWith("PLIVUS"))
                                        {
                                            if (curItem.Name.StartsWith("PLIVUS"))
                                            {
                                                if (!(File.Exists("c:/SuperSport/Cycling/Processed/" + curItem.Name)))
                                                {
                                                    curFtp.GetFile(curItem.Name, "c:/SuperSport/Cycling/Temp/" + curItem.Name);
                                                    curFtp.KeepAlive();
                                                    Downloaded += 1;
                                                }
                                            }
                                            else
                                            {
                                                curFtp.GetFile(curItem.Name, "c:/SuperSport/Cycling/Temp/" + curItem.Name);
                                                curFtp.KeepAlive();
                                                Downloaded += 1;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex1)
                                {
                                    _logger.Error("Error occurred in GetFiles()", ex1);

                                    fileErrors += 1;
                                    CurRun.Errors.Add(curItem.Name + " - " + ex1.Message);
                                }
                            }
                        }
                        CurRun.Downloads = Downloaded;
                        CurRun.DownloadErrors = fileErrors;
                    }

                    curFtp.Disconnect();
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in GetFiles()", ex);

                    CurRun.Errors.Add(ex.Message);
                }
                finally
                {
                    if (curFtp != null)
                    {
                        curFtp.Dispose();
                    }
                }
            }
        }

        private bool Data(XmlDocument xmlDoc)
        {
            SqlCommand SqlQuery;
            bool done = true;

            try
            {
                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("data"))
                {
                    string stageNumber = string.Empty;
                    string dataType = string.Empty;
                    
                    foreach (XmlAttribute attribute in xmlNode.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "stage":
                                stageNumber = attribute.Value;
                                break;
                            case "type":
                                dataType = attribute.Value;
                                break;
                            default:
                                break;
                        }
                    }

                    if (!String.IsNullOrEmpty(stageNumber))
                    {
                        int stageId = GetStageId(stageNumber);

                        if (dataType == "pressrelease")
                        {
                            foreach (XmlNode releaseNode in xmlNode.SelectNodes("pressrelease"))
                            {
                                DateTime posted = DateTime.Now;
                                string title = string.Empty;
                                string text = string.Empty;

                                foreach (XmlAttribute attribute in releaseNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "hour":
                                            string[] tmpTime = attribute.Value.Split(':');
                                            posted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(tmpTime[0]), Convert.ToInt32(tmpTime[1]), Convert.ToInt32(tmpTime[2]));
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                foreach (XmlNode detailNode in releaseNode.ChildNodes)
                                {
                                    switch (detailNode.Name)
                                    {
                                        case "title":
                                            title = detailNode.InnerText;
                                            break;
                                        case "text":
                                            text = detailNode.InnerText;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                var commentaryCheckCommand = new SqlCommand("Select Count(*) From SSZGeneral.dbo.tourdefrancelive where stageId = '"+ stageId +"' and title = '"+ title.Replace("'", "''").Trim() + "' and posted = '"+ posted + "'", DbConn);
                                var checkIfCommentExists = Convert.ToBoolean(commentaryCheckCommand.ExecuteScalar());

                                if (!checkIfCommentExists)
                                {
                                    SqlQuery = new SqlCommand("Insert Into SSZGeneral.dbo.tourdefrancelive (stageId, title, chapo, text, posted, created) Values (" + stageId + ", '" + title.Replace("'", "''").Trim() + "', '', '" + text.Replace("'", "''").Trim() + "', '" + posted + "', Getdate())", DbConn);
                                    SqlQuery.ExecuteNonQuery(); 
                                }
                            }
                        }
                        else
                        {
                            foreach (XmlNode standingNode in xmlNode.SelectNodes("standings"))
                            {
                                string type = string.Empty;
                                foreach (XmlAttribute attribute in standingNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "type":
                                            type = attribute.Value;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                StringBuilder sb = new StringBuilder();
                                switch (type)
                                {
                                    case "ITE":
                                        sb.Append("DELETE FROM SSZGeneral.dbo.tourdefranceresults WHERE (Stage = " + stageId + ");");
                                        break;
                                    case "IPG":
                                        sb.Append("DELETE FROM SSZGeneral.dbo.tourdefrancestandings WHERE (Stage = " + stageId + ") AND (Type = 'Sprint') AND (Tournament = " + Tournament + ");");
                                        break;
                                    case "IJG":
                                        sb.Append("DELETE FROM SSZGeneral.dbo.tourdefrancestandings WHERE (Stage = " + stageId + ") AND (Type = 'Youth') AND (Tournament = " + Tournament + ");");
                                        break;
                                    case "ETG":
                                        sb.Append("DELETE FROM SSZGeneral.dbo.tourdefrancestandings WHERE (Stage = " + stageId + ") AND (Type = 'Team') AND (Tournament = " + Tournament + ");");
                                        break;
                                    case "ITG":
                                        sb.Append("DELETE FROM SSZGeneral.dbo.tourdefrancestandings WHERE (Stage = " + stageId + ") AND (Type = 'Individual') AND (Tournament = " + Tournament + ");");
                                        break;
                                    case "IMG":
                                        sb.Append("DELETE FROM SSZGeneral.dbo.tourdefrancestandings WHERE (Stage = " + stageId + ") AND (Type = 'Climb') AND (Tournament = " + Tournament + ");");
                                        break;
                                    default:
                                        break;
                                }

                                foreach (XmlNode rankNode in standingNode.SelectNodes("rank"))
                                {
                                    int position = 0;
                                    string number = string.Empty;
                                    string gap = "1/1/1900 00:00:00.000";
                                    string time = "00:00:00.000";
                                    int points = 0;

                                    foreach (XmlAttribute attribute in rankNode.Attributes)
                                    {
                                        switch (attribute.Name)
                                        {
                                            case "position":
                                                bool result1 = Int32.TryParse(attribute.Value, out position);
                                                break;
                                            case "number":
                                                number = attribute.Value;
                                                break;
                                            case "time":
                                                if (attribute.Value.IndexOf(":") >= 0)
                                                {
                                                    time = attribute.Value;
                                                }
                                                break;
                                            case "gap":
                                                if (attribute.Value.IndexOf(":") >= 0)
                                                {
                                                    gap = "1/1/1900 " + attribute.Value;
                                                }
                                                break;
                                            case "points":
                                                bool result2 = Int32.TryParse(attribute.Value, out points);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    switch (type)
                                    {
                                        case "ITE":
                                            sb.Append("INSERT INTO SSZGeneral.dbo.tourdefranceresults (Stage, Pos, Number, Time, Gap, Created) VALUES (" + stageId + ", " + position + ", '" + number.Replace("'", "''").Trim() + "', '" + time + "', '" + gap + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                                            break;
                                        case "IPG":
                                            sb.Append("INSERT INTO SSZGeneral.dbo.tourdefrancestandings (tournament, Stage, Type, Pos, Number, Time, Gap, Points, Created) VALUES (" + Tournament + ", " + stageId + ", 'Sprint', " + position + ", '" + number.Replace("'", "''").Trim() + "', '" + time + "', '" + gap + "', '" + points + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                                            break;
                                        case "IJG":
                                            sb.Append("INSERT INTO SSZGeneral.dbo.tourdefrancestandings (tournament, Stage, Type, Pos, Number, Time, Gap, Points, Created) VALUES (" + Tournament + ", " + stageId + ", 'Youth', " + position + ", '" + number.Replace("'", "''").Trim() + "', '" + time + "', '" + gap + "', 0, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                                            break;
                                        case "ETG":
                                            sb.Append("INSERT INTO SSZGeneral.dbo.tourdefrancestandings (tournament, Stage, Type, Pos, Number, Time, Gap, Points, Created) VALUES (" + Tournament + ", " + stageId + ", 'Team', " + position + ", '" + number.Replace("'", "''").Trim() + "', '" + time + "', '" + gap + "', 0, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                                            break;
                                        case "ITG":
                                            sb.Append("INSERT INTO SSZGeneral.dbo.tourdefrancestandings (tournament, Stage, Type, Pos, Number, Time, Gap, Points, Created) VALUES (" + Tournament + ", " + stageId + ", 'Individual', " + position + ", '" + number.Replace("'", "''").Trim() + "', '" + time + "', '" + gap + "', 0, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                                            break;
                                        case "IMG":
                                            sb.Append("INSERT INTO SSZGeneral.dbo.tourdefrancestandings (tournament, Stage, Type, Pos, Number, Time, Gap, Points, Created) VALUES (" + Tournament + ", " + stageId + ", 'Climb', " + position + ", '" + number.Replace("'", "''").Trim() + "', '" + time + "', '" + gap + "', '" + points + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                if (sb.ToString().IndexOf("INSERT") >= 0)
                                {
                                    SqlQuery = new SqlCommand(sb.ToString(), DbConn);
                                    SqlQuery.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (dataType == "stages")
                        {
                            foreach (XmlNode stagesNode in xmlNode.SelectNodes("stages"))
                            {
                                foreach (XmlNode stageNode in stagesNode.ChildNodes)
                                {
                                    var createdDate = DateTime.Now;
                                    var number = string.Empty;
                                    var type = string.Empty;
                                    var distance = string.Empty;
                                    DateTime stageDate = new DateTime();
                                    var departureTown = string.Empty;
                                    var arrivalTown = string.Empty;
                                    var stage = string.Empty;

                                    foreach (XmlAttribute stageAttribute in stageNode.Attributes)
                                    {
                                        switch (stageAttribute.Name)
                                        {
                                            case "number":
                                                number = stageAttribute.Value;
                                                stage = number.Substring(0, 2).TrimStart('0');
                                                break;
                                            case "type":
                                                type = getStageType(stageAttribute.Value);
                                                break;
                                            case "distance":
                                                distance = stageAttribute.Value;
                                                break;
                                            case "date":
                                                string[] tmpDate = stageAttribute.Value.Split('/');
                                                stageDate = new DateTime(Convert.ToInt32(tmpDate[2]),
                                                    Convert.ToInt32(tmpDate[1]), Convert.ToInt32(tmpDate[0]));
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    if (stage == "") continue;

                                    foreach (XmlNode townNode in stageNode.ChildNodes)
                                    {
                                        if (townNode.Attributes[0].Name == "type" &&
                                            townNode.Attributes[0].Value == "D")
                                        {
                                            departureTown = townNode.Attributes[1].Value;
                                        }
                                        else if (townNode.Attributes[0].Name == "type" &&
                                                 townNode.Attributes[0].Value == "A")
                                        {
                                            arrivalTown = townNode.Attributes[1].Value;
                                        }
                                    }

                                    //check if stage exists
                                    SqlQuery = new SqlCommand("Select Count(*) from SSZGeneral.dbo.TourdefranceStages Where Tournament = " + Tournament + " and Number = " + number, DbConn);
                                    var stageExists = Convert.ToBoolean(SqlQuery.ExecuteScalar());

                                    if (!stageExists)
                                    {
                                        using (SqlCommand Command = new SqlCommand("Insert Into SSZGeneral.dbo.TourdefranceStages (Tournament, Number, Departure, Arrival, Distance, Type, StartDate, StageNumber, Created) Values ( @Tournament,@number,@departureTown,@arrivalTown, @distance ,@type, @stageDate ,@stage,@Created)",DbConn))


                                        {

                                            Command.Parameters.AddWithValue("@Tournament", Tournament);
                                            Command.Parameters.AddWithValue("@number", number);
                                            Command.Parameters.AddWithValue("@departureTown", departureTown);
                                            Command.Parameters.AddWithValue("@arrivalTown", arrivalTown);
                                            Command.Parameters.AddWithValue("@distance", distance);
                                            Command.Parameters.AddWithValue("@type", type);
                                            Command.Parameters.AddWithValue("@stageDate", stageDate);
                                            Command.Parameters.AddWithValue("@stage", stage);
                                            Command.Parameters.AddWithValue("@Created", System.DateTime.Now);
                                       

                                       // DbConn.Open();
                                        Command.ExecuteNonQuery();
                                            
                                        }
                                        //SqlQuery = new SqlCommand("Insert Into SSZGeneral.dbo.TourdefranceStages (Tournament, Number, Departure, Arrival, Distance, Type, StartDate, StageNumber, Created) " +
                                        //                          "Values (" + Tournament + ", '" + number + "', '" + departureTown + "', '" + arrivalTown + "', '" + distance + "', '" + type + "', '" + stageDate + "', '" + stage + "', Getdate())", DbConn);
                                        //SqlQuery.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                        else
                        if (dataType == "starters")
                        {
                            foreach (XmlNode ridersNode in xmlNode.SelectNodes("riders"))
                            {
                                foreach (XmlNode teamNode in ridersNode.ChildNodes)
                                {
                                    var number = string.Empty;
                                    var code = string.Empty;
                                    var name = string.Empty;
                                    var director = string.Empty;

                                    foreach (XmlAttribute teamAttribute in teamNode.Attributes)
                                    {
                                        switch (teamAttribute.Name)
                                        {
                                            case "number":
                                                number = teamAttribute.Value;
                                                break;
                                            case "code":
                                                code = teamAttribute.Value;
                                                break;
                                            case "name":
                                                name = teamAttribute.Value;
                                                break;
                                            case "director":
                                                director = teamAttribute.Value;
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    //check if stage exists
                                    SqlQuery = new SqlCommand("Select Count(*) from SSZGeneral.dbo.Tourdefranceteams Where Tournament = " + Tournament + " and Number = " + number, DbConn);
                                    var teamExists = Convert.ToBoolean(SqlQuery.ExecuteScalar());

                                    if (!teamExists)
                                    {
                                        SqlQuery = new SqlCommand("Insert Into SSZGeneral.dbo.Tourdefranceteams (Tournament, Number, Name, Code, Shortname, Mobilename, Director, Created) " +
                                                                  "Values (" + Tournament + ", '" + number + "', '" + name + "', '" + code + "', '" + code + "', '" + code + "', '" + director + "', Getdate())", DbConn);
                                        SqlQuery.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in Data()", ex);

                CurRun.Errors.Add(ex.Message);
                done = false;
            }

            return done;
        }

        private string getStageType(string attributeValue)
        {
            var type = string.Empty;
            switch (attributeValue)
            {
                case "PAS":
                    type = "Individual Time Trial";
                    break;
                case "PLN":
                    type = "Flat (Stage)";
                    break;
                case "VAL":
                    type = "Hilly (Stage)";
                    break;
                case "HMG":
                    type = "Mountain (Stage)";
                    break;
                case "REP":
                    type = "Rest Day";
                    break;
                default:
                    break;
            }
            return type;
        }

        private int GetStageId(string stageNumber)
        {
            int stageId = -1;

            SqlCommand SqlQuery = new SqlCommand("SELECT TOP 1 Id FROM SSZGeneral.dbo.tourdefrancestages WHERE (tournament = @tournament) AND (number = @number)", DbConn);
            SqlQuery.Parameters.Add("@tournament", SqlDbType.Int).Value = Tournament;
            SqlQuery.Parameters.Add("@number", SqlDbType.Int).Value = stageNumber;
            stageId = Convert.ToInt32(SqlQuery.ExecuteScalar());

            return stageId;
        }

        private void WriteInfo()
        {
            try
            {
                FileStream Fs = new FileStream(ConfigurationManager.AppSettings["log"], FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + CurRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + CurRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("Downloads: " + CurRun.Downloads.ToString());
                Sw.WriteLine("Download Errors: " + CurRun.DownloadErrors.ToString());
                foreach (string tmpString in CurRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in WriteInfo()", ex);
            }
        }

        private void EndRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in CurRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                //SqlCommand SqlQuery = new SqlCommand("Insert Into cycling.dbo.leaderboardIngest (tournament, startTime, endTime, errors, timeStamp) Values (0, @startTime, @endTime, @errors, @timeStamp)", dbConn);
                //SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                //SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                //SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                //SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                //SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in EndRun()", ex);

                CurRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }
    }
}